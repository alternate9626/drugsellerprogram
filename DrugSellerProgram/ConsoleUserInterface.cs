﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class ConsoleUserInterface : UserInterface
    {
        public void ShowWelcomeMessage()
        {
            Console.WriteLine("Welcome to the DrugSellerProgram!");
        }

        public void ShowExitMessage()
        {
            Console.WriteLine("Press any key then 'enter' to exit...");
            Console.ReadLine();
        }

        public void AskForCreditCardNumber()
        {
            Console.WriteLine("Enter Credit Card Number");            
        }

        public String GetCreditCardNumber()
        {
            return Console.ReadLine();
        }

        public void ShowCreditCardNumberNotValid()
        {
            Console.WriteLine("Not a valid credit card number.");
        }

        public void ShowAfterPaymentMessage(BankNames bankName)
        {
            Console.WriteLine("Payment went trough " + bankName + " Services.");
            Console.WriteLine("Thanks for your payment! You can now exit.");
        }

        public void ShowInvalidIINMessage()
        {
            Console.WriteLine("IIN Not Recognized. Payment cannot proceed.");
        }

        public void ShowProcessMessage(String name)
        {
            Console.WriteLine("Proccessed trough " + name + " services!");
        }


    }
}

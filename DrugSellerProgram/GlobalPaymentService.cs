﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class GlobalPaymentService : PaymentService
    {
        private String name = "Global payments";
        private UserInterface ui;

        public GlobalPaymentService(UserInterface ui)
        {
            this.ui = ui;
        }
        
        public void Process()
        {
            ui.ShowProcessMessage(this.name);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class PaymentServiceFactory
    {
        private UserInterface ui;

        public PaymentServiceFactory(UserInterface ui)
        {
            this.ui = ui;
        }

        public PaymentService Create(BankNames bankName)
        {
            PaymentService paymentService = new PaypalPaymentService(ui);
            switch (bankName)
            {
                case BankNames.VISA:
                    paymentService = new PaypalPaymentService(ui);
                    break;
                case BankNames.MASTERCARD:
                    paymentService = new MonerisPaymentService(ui);
                    break;
                case BankNames.DISCOVERY:
                    paymentService = new GlobalPaymentService(ui);
                    break;                   
            }
            return paymentService;            
        }
    }
}

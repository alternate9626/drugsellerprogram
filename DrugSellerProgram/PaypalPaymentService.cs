﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class PaypalPaymentService : PaymentService
    {
        private String name = "Paypal";
        private UserInterface ui;

        public PaypalPaymentService(UserInterface ui)
        {
            this.ui = ui;
        }
        
        public void Process()
        {
            ui.ShowProcessMessage(this.name);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class MonerisPaymentService : PaymentService
    {
        private String name = "Moneris";
        private UserInterface ui;

        public MonerisPaymentService(UserInterface ui)
        {
            this.ui = ui;
        }
        
        public void Process()
        {
            ui.ShowProcessMessage(this.name);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class DrugSellerProgramStarter
    { 
        static void Main(string[] args)
        {
            UserInterface ui = new ConsoleUserInterface();
            CreditCardNumberGetter creditCardNumberGetter = new CreditCardNumberGetter(ui);
            BankNameGetter bankNameGetter = new BankNameGetter();
            DrugSellerProgram drugSellerProgram = new DrugSellerProgram(ui,creditCardNumberGetter,bankNameGetter);
            drugSellerProgram.Start();            
        }
    }
}

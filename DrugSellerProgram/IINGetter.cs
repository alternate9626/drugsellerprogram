﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class BankNameGetter
    {     
        public BankNames Get(String creditCardNumber)
        {
            return GetBankNameFromCreditCardNumber(creditCardNumber);            
        }

        private BankNames GetBankNameFromCreditCardNumber(String creditCardNumber)
        {            
            switch (creditCardNumber[0])
            {                
                case '4':
                    return BankNames.VISA;                    
                case '5':
                   return BankNames.MASTERCARD;
                    
                case '6':
                    return BankNames.DISCOVERY;                    
                default:
                    return BankNames.N_A;                             
            }            
        }
    }
}

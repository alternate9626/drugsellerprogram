﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    interface UserInterface
    {
        void ShowWelcomeMessage();

        void ShowExitMessage();

        void AskForCreditCardNumber();

        String GetCreditCardNumber();

        void ShowCreditCardNumberNotValid();

        void ShowAfterPaymentMessage(BankNames bankName);

        void ShowInvalidIINMessage();

        void ShowProcessMessage(String name);

    }
}

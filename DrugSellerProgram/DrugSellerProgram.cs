﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class DrugSellerProgram
    {
        private UserInterface ui;
        private String creditCardNumber;
        private CreditCardNumberGetter creditCardNumberGetter;
        private BankNames bankName;
        private BankNameGetter bankNameGetter;
        private PaymentService paymentService;
        private PaymentServiceFactory paymentServiceGetter;   

        public DrugSellerProgram(UserInterface ui, CreditCardNumberGetter creditCardNumberGetter, BankNameGetter bankNameGetter)
        {
            this.creditCardNumberGetter = creditCardNumberGetter;
            this.bankNameGetter = bankNameGetter;            
            this.ui = ui;
        }

        public void Start()
        {            
            ui.ShowWelcomeMessage();               
            creditCardNumber = creditCardNumberGetter.Get();           
            bankName = bankNameGetter.Get(creditCardNumber);
            paymentServiceGetter = new PaymentServiceFactory(ui);
            paymentService = paymentServiceGetter.Create(bankName);
            
            if (IsBankNameValid())
            {
                paymentService.Process();
                ui.ShowAfterPaymentMessage(bankName);
            }

            else
            {
                ui.ShowInvalidIINMessage();
            }

            ui.ShowExitMessage();
        }      

        private bool IsBankNameValid()
        {
            return bankName != BankNames.N_A;
        }
    }
}



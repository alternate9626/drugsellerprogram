﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugSellerProgram
{
    class CreditCardNumberGetter
    {           
        private UserInterface ui;

        public CreditCardNumberGetter(UserInterface ui)
        {            
            this.ui = ui;            
        }

        public String Get()
        {            
           ui.AskForCreditCardNumber();
           return GetValidCreditCardNumber();   
        }

        private String GetValidCreditCardNumber()
        {            
            String creditCardNumber = ui.GetCreditCardNumber();
            while (!IsCreditCardNumber16Digits(creditCardNumber))
            {
                ui.ShowCreditCardNumberNotValid();
                ui.AskForCreditCardNumber();
                creditCardNumber = ui.GetCreditCardNumber();
            }
            return creditCardNumber;
        }

        private bool IsCreditCardNumber16Digits(String creditCardNumber)
        {
            return creditCardNumber.Length == 16;
        }

        
    }
}
